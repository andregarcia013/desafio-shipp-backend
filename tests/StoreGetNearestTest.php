<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class StoreGetNearestTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->call('GET', '/v1/stores?latitude=42.73078&longitude=-73.701397');

        $this->assertEquals(
            200,
            $response->status()
        );
    }
}
