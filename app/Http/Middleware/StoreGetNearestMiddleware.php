<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class StoreGetNearestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->get('latitude') > 90 || $request->get('latitude') < -90 || $request->get('longitude') > 180 || $request->get('longitude') < -180 ) {
            abort(400, 'Invalid Parameters');
        }


        return $next($request);
    }

    protected function log($request)
    {

        Log::info('====================== StoreGetNearestStore Called ========================');
        Log::info('URL: ' . $request->fullUrl());
        Log::info('Method: ' . $request->getMethod());
        Log::info('Status Code: ' . $request->getStatusCode());
        Log::info('IP Address: ' . $request->getClientIp());
    }
}
