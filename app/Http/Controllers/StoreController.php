<?php

namespace App\Http\Controllers;

use App\Helpers\Position;
use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{

    public function __construct()
    {
    }

    public function getNearestStores(Request $request)
    {

        $currentPosition = new Position(floatval($request->latitude), floatval($request->longitude));

        return response()->json(Store::getNearestStores($currentPosition));
    }
}
