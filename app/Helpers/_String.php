<?php


namespace App\Helpers;


class _String
{
    /**
     * Este atribute é referente a String que será manipulada.
     *
     * @var string
     */
    private $string;



    public function __construct($string)
    {
        $this->string = $string;
    }

    /**
     * Retorna a string formatada para um formatado válido .
     *
     *
     *@return string Retorna a string formatada para um formatado válido
     */
    public function formatToAValidJson()
    {
        $this->string = str_replace("'{\"", '{"', $this->string);
        $this->string = str_replace("\"}'", '"}', $this->string);
        $this->string = str_replace("'", '"', $this->string);
        $this->string = str_replace("False", 'false', $this->string);

        return $this->string;
    }
}
