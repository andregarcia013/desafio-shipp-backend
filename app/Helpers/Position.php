<?php


namespace App\Helpers;


class Position
{
    /**
     * Este atribute é referente a latitude de um ponto fixo no qual está referenciando a distancia.
     *
     * @var double
     */
    private $latitude;

    /**
     * Este atribute é referente a longitude de um ponto fixo no qual está referenciando a distancia.
     *
     * @var double
     */
    private $longitude;

    public function __construct($latitude, $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * Compara a distancia fixa do objeto com uma distancia de referancia.
     *
     * @param double $latitude  Latitude para ser Comparada com a posição fixa do Objeto
     * @param double $longitude Longitude para ser Comparada com a posição fixa do Objeto
     *
     *@return double Retorna a distancia em metros
     */
    public function getDistance($latitude, $longitude)
    {
        $x = deg2rad($this->longitude - $longitude) * cos(deg2rad($this->longitude));
        $y = deg2rad($this->latitude - $latitude);
        $dist = 6371000.0 * sqrt($x * $x + $y * $y);
        return $dist;
    }

    /**
     * Compara a distancia fixa do objeto com uma distancia de referancia.
     *
     * @param double $latitude  Latitude para ser Comparada com a posição fixa do Objeto
     * @param double $longitude Longitude para ser Comparada com a posição fixa do Objeto
     *
     *@return double Retorna a distancia em Kilometros
     */
    public function getDistanceInKilometers($latitude, $longitude)
    {
        $x = deg2rad($this->longitude - $longitude) * cos(deg2rad($this->longitude));
        $y = deg2rad($this->latitude - $latitude);
        $dist = 6371000.0 * sqrt($x * $x + $y * $y);
        return $dist / 1000;
    }
}
