<?php

namespace App;

use App\Helpers\_String;
use Illuminate\Database\Eloquent\Model;


class Store extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "stores";

    /**
     * Compara a distancia fixa do objeto com uma distancia de referancia.
     *
     * @param Position Objeto Position ( $latitude,  $longitude )
     * @param double $distance Distancia minima do objecto fixo.
     * @param ASC DESC $order Ordem de organização das lojas

     *
     *@return array Retorna todas as lojas mais proximas e ordenadas
     */

    public static function getNearestStores($currentPosition, $distance = 6.5, $order = "ASC")
    {
        $stores = Store::all();
        $results = [];
        foreach ($stores as $store) {
            $str = new _String($store->location);
            $store->location = $str->formatToAValidJson();
            $store->location  = json_decode($store->location);

            if (isset($store->location->latitude) && isset($store->location->longitude)) {
                $store->distance = $currentPosition->getDistanceInKilometers(floatval($store->location->latitude), floatval($store->location->longitude));
                if ($store->distance < $distance) {
                    $results[] = $store;
                }
            }
        }

        $order == "ASC" ?  usort($results, function ($a, $b) {
            return $a->distance > $b->distance;
        }) :  usort($results, function ($a, $b) {
            return $a->distance < $b->distance;
        });

        return $results;
    }
}
